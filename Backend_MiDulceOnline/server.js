//Importamos módulo de express
// forma convencional de node.js
const express = require ('express');
//otra forma de JavaScrip
// import express from 'express'

//Importar mongoose
const mongoose = require('mongoose');

//Importar las variables de entorno
require('dotenv').config({path: 'var.env'});

//Para establecer las rutas de express
const router = express.Router();

//Importar la ruta de la configuración con la base de datos
const conectarDB = require('./config/cxn_db');

//Inicializar express
var app = express();
app.use(express.json());
//Importar CORS
const cors = require('cors');
app.use(cors());

var whitelist = ['http://localhost:3000/', 'http://localhost:4000/']
 var corsOptionsDelegate = function (req, callback){
     var corsOptions;
     if (whitelist.indexOf(req.header('Origin'))!== -1){
        corsOptions = {Origin: true}
    }else{
         corsOptions = {Origin : false}
     }
     callback(null,corsOptions)
 }


//Establecer la conexión con la base de datos

conectarDB();

//Especificamos que en la carpera "public" está contenido el frontend.
app.use(express.static('public'));

//-----------------------------------------------------------------
//Prueba mínima para comprobar el módulo de express
//En relación al entorno web (ejecutar el servidor eb)
//app.use('/', function(req, res){
    //res.send("Bienvenidos")
//});

//Utilizando una funcion flecha
//app.use('/', (req, res) => {
    //res.send("Utilizando la funcion flecha")
//});
//-----------------------------------------------------------------


//-----------------------------------------------------------------
//Para utilizar los metodos http => GET, POST, PUT, DELTE, etc...
//const router = express.Router(); => agregada en la linea 8 antes de inicializar express
app.use(router);

//Importar el controlador de proovedor => CRUD
const controlProveedor = require('./controllers/controlProveedor');

router.post('/apirest/proveedor/',cors(corsOptionsDelegate), controlProveedor.crear);         //Create
router.get('/apirest/proveedor/',cors(corsOptionsDelegate), controlProveedor.obtener);        //Read
router.put('/apirest/proveedor/:id',cors(corsOptionsDelegate), controlProveedor.actualizar);  //Update
router.delete('/apirest/proveedor/:id',cors(corsOptionsDelegate), controlProveedor.eliminar); //Delete

// router.post('/apirest/proveedor/', controlProveedor.crear);         //Create
// router.get('/apirest/proveedor/', controlProveedor.obtener);        //Read
// router.put('/apirest/proveedor/:id', controlProveedor.actualizar);  //Update
// router.delete('/apirest/proveedor/:id', controlProveedor.eliminar); //Delete


//Importar el controlador de categoria => CRUD
const controlCategoria = require('./controllers/controlCategoria');

router.post('/apirest/categoria',cors(corsOptionsDelegate), controlCategoria.crear); 
router.get('/apirest/categoria',cors(corsOptionsDelegate), controlCategoria.obtener);
router.put('/apirest/categoria/:id',cors(corsOptionsDelegate), controlCategoria.actualizar);
router.delete('/apirest/categoria/:id',cors(corsOptionsDelegate), controlCategoria.eliminar); 

//Importar el controlador de producto => CRUD
const controlProducto = require('./controllers/controlProducto');

router.post('/apirest/producto',cors(corsOptionsDelegate), controlProducto.crear); 
router.get('/apirest/producto',cors(corsOptionsDelegate), controlProducto.obtener); 
router.put('/apirest/producto/:id',cors(corsOptionsDelegate), controlProducto.actualizar); 
router.delete('/apirest/producto/:id',cors(corsOptionsDelegate), controlProducto.eliminar);

// router.get('/mensaje', function(req, res){
//     res.send('Mensaje con el método GET');

    //const name_db = 'MiDulceOnline'
    //const user = 'sezb'
    //const psw = '12345zx'
    //const uri = `mongodb+srv://${user}:${psw}@misiontic-u31.3hbw67x.mongodb.net/${name_db}?retryWrites=true&w=majority`
    //const uri = 'mongodb+srv://sezb:12345zx@misiontic-u31.3hbw67x.mongodb.net/MiDulceOnline?retryWrites=true&w=majority'
    //mongoose.connect(uri)

    //mongoose.connect(process.env.URI_MONGODB)
    //.then(function(){console.log("Base de datos conectada")})
    //.catch(function(e){console.log("Error: " + e)})

    //Establecer la conexión con la base de datos
    //conectarDB();
// });

// router.post('/mensaje', function(req, res){
//     res.send('Mensaje con el método POST')
// });

// router.put('/mensaje', function(req, res){
//     res.send('Mensaje con el método PUT')
// });

// router.delete('/mensaje', function(req, res){
//     res.send('Mensaje con el método DELTE');
// });

//-----------------------------------------------------------------

//Asignación del puerto para el servidor web
//app.listen(2126);
app.listen(process.env.PORT);

//Impresión de mensaje en consola para confirmar que el servidor
//está activo

console.log('Servidor web en ejecución desde: http://localhost:4000/');
console.log('"Ctrl + C" para finalizar el servidor web');
