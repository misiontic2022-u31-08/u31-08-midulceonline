
//Importar mongoose
const mongoose = require('mongoose');
const { Schema } = mongoose;
//Establecer el esquema del documento en la colección
const proveedorSchema =Schema({
    nombre_prov :{
        type: String,
    },
    tlf_prov :{
        type: Number,
    },
    direccion_prov :{
        type: String,
    },
    ciudad_prov :{
        type: String,
    },
    img_prov :{
        type: String,
    }
},
{
    versionKey : false
});

//Exportar como módulo para que sea visible desde otros archivos/scripts
module.exports = mongoose.model('proveedores', proveedorSchema);
//para utilizar en el controlador => CRUD
