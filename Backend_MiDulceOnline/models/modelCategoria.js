
//Importar mongoose
const mongoose = require('mongoose');
const { Schema } = mongoose;
//Establecer el esquema del documento en la colección
const categoriaSchema =Schema({
    nombre_categ :{
        type: String,
    },
    img_categ:{
        type:String,
    }
},
{
    versionKey : false
});

//Exportar como módulo para que sea visible desde otros archivos/scripts
module.exports = mongoose.model('categorias', categoriaSchema);
