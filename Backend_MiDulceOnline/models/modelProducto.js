
//Importar mongoose
const mongoose = require('mongoose');
const { Schema } = mongoose;

//Establecer el esquema del documento en la colección
const productoSchema =Schema({
    nombre_prod :{
        type:String,
    },
    precio_prod :{
        type:Number,
    },
    descripcion_prod :{
        type:String,
    },
    id_proveedor : {type: Schema.Types.ObjectId, ref: 'proveedores'},
    id_categ_prod : {type: Schema.Types.ObjectId, ref: 'categorias'},
    inv_stock_prod :{
        type: Number,
    },
    img_prod:{
        type:String,
    }
},
{
    versionKey : false
});

//Exportar como módulo para que sea visible desde otros archivos/scripts
module.exports = mongoose.model('productos',productoSchema);
