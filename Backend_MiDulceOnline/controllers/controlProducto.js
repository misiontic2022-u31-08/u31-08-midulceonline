//Importar el nmodelo de proveedor
const { model, default: mongoose } = require('mongoose');
const modelProducto = require('../models/modelProducto');

//podemos exportar directamente cada uno de los métodos del CRUD:

//CRUD => Create
exports.crear = async (req, res) => {

    try {

        let producto;

        //Establecer los datos a guardar}
        producto = new modelProducto(req.body);
        //Guardamos en la base de datos
        await producto.save()

        //respuesta para verificar la variable
        res.send(producto);

    } catch (error) {

        console.log("Error al guardar los datos: " + error);
        
        //mensaje de error en la app
        res.status(500).send("Error al guardar el producto");

    }
}


//CRUD => Read
exports.obtener = async(req, res) => {

    try {
        
        //Consultar la base de datos
        const producto = await modelProducto.find().populate('id_proveedor').populate('id_categ_prod')
        res.json(producto)
        //Lo que retorna de la base de datos en la variable proovedor
        //lo convierto a json
        

    } catch (error) {

        console.log("Error al leer los datos: " + error);
        
        //mensaje de error en la app
        res.status(500).send("Error al leer los datos del producto");
    }
}


//CRUD => Update
exports.actualizar = async (req, res) => {

    try {
        //Validar que exista el resgistro a actualizar 
        const producto = await modelProducto.findById(req.params.id);

        //Validar si no existe el proveedor
        if (!producto){
            res.status(404).json({mensaje: "El producto no existe"});
        } 
        
        else{

            //actualiza los datos
            await modelProducto.findByIdAndUpdate({_id: req.params.id}, req.body);

            res.json({mensaje: "El producto fue actualizado"})
        }


    } catch (error) {

        console.log("Error al actualizar los datos: " + error);
        
        //mensaje de error en la app
        res.status(500).send("Error al actualizar los datos del producto");
    }

}

//CRUD => Delete
exports.eliminar = async (req, res) => {

    try {

        //Validar que el registro a eliminar existe
        const producto = await modelProducto.findById(req.params.id);

        //Validar si el proveedor no existe
        if (!producto){
            res.status(404).json({mensaje: 'El producto no existe'})
        }
        else{
            await modelProducto.findByIdAndRemove({_id: req.params.id});
            res.json({mensaje: "Producto eliminada correctamente"});
        }
        
    } catch (error) {

        console.log("Error al borrar los datos: " + error);
        
        //mensaje de error en la app
        res.status(500).send("Error al borrar los datos del producto");
    }
}