
//Importar el nmodelo de proveedor
const modelProveedor = require('../models/modelProveedor');

//podemos exportar directamente cada uno de los métodos del CRUD:

//CRUD => Create
exports.crear = async (req, res) => {

    try {
        
        let proveedor;

        //Establecer los datos a guardar}
        console.log("req.body: " + req.body);
        proveedor = new modelProveedor(req.body);
        console.log("proveedor: " + proveedor);

        //Guardamos en la base de datos
        await proveedor.save()

        //respuesta para verificar la variable
        res.send(proveedor);

    } catch (error) {

        console.log("Error al guardar los datos: " + error);
        
        //mensaje de error en la app
        res.status(500).send("Error al guardar el proveedor");

    }
}


//CRUD => Read
exports.obtener = async(req, res) => {

    try {
        
        //Consultar la base de datos
        const proveedor = await modelProveedor.find();

        //Lo que retorna de la base de datos en la variable proovedor
        //lo convierto a json
        res.json(proveedor);

    } catch (error) {

        console.log("Error al leer los datos: " + error);
        
        //mensaje de error en la app
        res.status(500).send("Error al leer los datos del proveedor");
    }
}


//CRUD => Update
exports.actualizar = async (req, res) => {

    try {
        console.log("req.body: " + req.body);
        //Validar que exista el resgistro a actualizar 
        const proveedor = await modelProveedor.findById(req.params.id);
        console.log("proveedor: " + proveedor);

        //Validar si no existe el proveedor
        if (!proveedor){
            res.status(404).json({mensaje: "El proveedor no existe"});
        } 
        
        else{

            //actualiza los datos
            console.log("req.body: " + req.body);
            await modelProveedor.findByIdAndUpdate({_id: req.params.id}, req.body);
            //await modelProveedor.findByIdAndUpdate({_id: req.params.id}, {nombre_prov: "Maria"});

            res.json({mensaje: "El proveedor fue actualizado"})
        }


    } catch (error) {

        console.log("Error al actualizar los datos: " + error);
        
        //mensaje de error en la app
        res.status(500).send("Error al actualizar los datos del proveedor");
    }

}


//CRUD => Delete
exports.eliminar = async (req, res) => {

    try {

        //Validar que el registro a eliminar existe
        const proovedor = await modelProveedor.findById(req.params.id);

        //Validar si el proveedor no existe
        if (!proovedor){
            res.status(404).json({mensaje: 'El proveedor no existe'})
        }
        else{
            await modelProveedor.findByIdAndRemove({_id: req.params.id});
            res.json({msg: "Proveedor eliminado"});
        }
        
    } catch (error) {

        console.log("Error al borrar los datos: " + error);
        
        //mensaje de error en la app
        res.status(500).send("Error al borrar los datos del proveedor");
    }
}
