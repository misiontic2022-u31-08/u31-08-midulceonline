//Importar el nmodelo de proveedor
const { model } = require('mongoose');
const modelCategoria = require('../models/modelCategoria');

//podemos exportar directamente cada uno de los métodos del CRUD:

//CRUD => Create
exports.crear = async (req, res) => {

    try {
        
        let categoria;

        //Establecer los datos a guardar}
        categoria = new modelCategoria(req.body);

        //Guardamos en la base de datos
        await categoria.save()

        //respuesta para verificar la variable
        res.send(categoria);

    } catch (error) {

        console.log("Error al guardar los datos: " + error);
        
        //mensaje de error en la app
        res.status(500).send("Error al guardar la categoria");

    }
}


//CRUD => Read
exports.obtener = async(req, res) => {

    try {
        
        //Consultar la base de datos
        const categoria = await modelCategoria.find();

        //Lo que retorna de la base de datos en la variable proovedor
        //lo convierto a json
        res.json(categoria);

    } catch (error) {

        console.log("Error al leer los datos: " + error);
        
        //mensaje de error en la app
        res.status(500).send("Error al leer los datos de la categoria");
    }
}


//CRUD => Update
exports.actualizar = async (req, res) => {

    try {
        //Validar que exista el resgistro a actualizar 
        const categoria = await modelCategoria.findById(req.params.id);

        //Validar si no existe el proveedor
        if (!categoria){
            res.status(404).json({mensaje: "La categoria no existe"});
        } 
        
        else{

            //actualiza los datos
            await modelCategoria.findByIdAndUpdate({_id: req.params.id}, req.body);

            res.json({mensaje: "La categoria fue actualizada"})
        }


    } catch (error) {

        console.log("Error al actualizar los datos: " + error);
        
        //mensaje de error en la app
        res.status(500).send("Error al actualizar los datos de la categoria");
    }

}

//CRUD => Delete
exports.eliminar = async (req, res) => {

    try {

        //Validar que el registro a eliminar existe
        const categoria = await modelCategoria.findById(req.params.id);

        //Validar si el proveedor no existe
        if (!categoria){
            res.status(404).json({mensaje: 'La categoria no existe'})
        }
        else{
            await modelCategoria.findByIdAndRemove({_id: req.params.id});
            res.json({mensaje: "Categoria eliminada correctamente"});
        }
        
    } catch (error) {

        console.log("Error al borrar los datos: " + error);
        
        //mensaje de error en la app
        res.status(500).send("Error al borrar los datos de la vategoria");
    }
}
