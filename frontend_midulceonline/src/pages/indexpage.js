import Carousel from "../components/carousel";
import Bienvenida from "../components/bienvenida";

const IndexPage = () =>{

    return (
<main>
  <section className="inicio">
  <Carousel/>
  <Bienvenida/>

  </section>       
</main>

    );
}

export default IndexPage;
