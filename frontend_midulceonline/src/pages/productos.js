
import Producto from "../components/Producto";


const ProductosPage = () => {
    return (
        <main>
        <section className="productos">
        <h2 className="productos-title">Productos</h2>
        <div className="productos-cards">
        <Producto />
        </div>
    </section>
     
        </main>

    );
}

export default ProductosPage;