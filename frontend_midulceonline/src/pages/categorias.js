import Categoria from "../components/categoria";

const CategoriasPage = () => {
    return (
        <main>
            <section className="categorias">
                <h2 className="categorias-title">Categorias</h2>
                <div className="categorias-cards">
                    <Categoria />
                </div>
            </section>
        </main>
    )

}

export default CategoriasPage;