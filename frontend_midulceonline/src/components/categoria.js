import { useState } from "react";
const Categoria = () => {
    const [categorias, setCategoria]=useState([]);
  const cargarCategoria = () =>{
          fetch('https://midulceonline-candyparty.herokuapp.com/apirest/categoria/')
              .then(res => res.json())
              .then(cadaCat =>setCategoria(cadaCat));
  }
  cargarCategoria();
    return (
        categorias.map(cadaCat => {
            return (
                <div className="categ-card">
                    <img src={cadaCat.img_categ} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h1 className="card-text">{cadaCat.nombre_categ}</h1>
                        <button className="cssbuttons-io-button"> Ver Productos
                            <div className="icon">
                                <svg height="24" width="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h24v24H0z" fill="none"></path><path d="M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z" fill="currentColor"></path></svg>
                            </div>
                        </button>
                    </div>
                </div>

            )
        })
    )
}
export default Categoria;
