import Nav from "./nav";
const Header = () => {
    return (
<header className="header"> 
  <div className="header-contenido">
    <img className="contenido-img" src="https://cdn-icons-png.flaticon.com/512/688/688297.png" alt="logo" />
    <div className="contenido-info">
      <h1 className="info-title">Candy Party</h1>
      <p className="info-slogan">Damos alegria</p>
    </div>
  </div>
  <Nav />
</header>

    );
}
export default Header;