import { NavLink } from "react-router-dom";
const Nav = () => {
  return (
    <ul className="nav">
      <li className="nav-item">
        <NavLink className="nav-link" to="/">
          Inicio
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/listaProductos">
          Productos
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/listaCategorias">
          Categorias
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/listaProveedores">
          Nuestros Proveedores
        </NavLink>
      </li>
    </ul>
  );
};

export default Nav;
