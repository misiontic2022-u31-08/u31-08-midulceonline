
const footer = () => {
    return (
<footer className="footer">
    <div className="nav-contenido">
      <img className="contenido-img" src="https://cdn-icons-png.flaticon.com/512/688/688297.png" alt="logo" />
      <div className="contenido-info">
        <h1 className="info-title">Candy Party</h1>
        <p className="info-slogan">Damos alegria</p>
      </div>
    </div>
    <div className="footer-Redes">
    <div>
      <ul className="wrapper">
        <li className="icon facebook">
          <span className="tooltip">Facebook</span>
          <span><i className="fab fa-facebook-f" /></span>
        </li>
        <li className="icon twitter">
          <span className="tooltip">Twitter</span>
          <span><i className="fab fa-twitter" /></span>
        </li>
        <li className="icon instagram">
          <span className="tooltip">Instagram</span>
          <span><i className="fab fa-instagram" /></span>
        </li>
      </ul>
    </div>
  </div>
  
</footer>

    );
}
export default footer;