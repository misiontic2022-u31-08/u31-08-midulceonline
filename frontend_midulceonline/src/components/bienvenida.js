const Carrousel = () => {
    return (
    <div className="inicio-bienvenida">
      <h1 className="bienvenida-title">Bienvenidos</h1>
      <p className="bienvenida-descripcion">Te encuentras en un mundo llenos de delicias y sabores que haran explotar tu paladar</p>
      <a className="bienvenida-btn">Conoce nuestros proveedores</a>
    </div>
    )}
    export default Carrousel;