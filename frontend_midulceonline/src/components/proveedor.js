import { useState } from "react";

const Proveedor = () => {

  const [proveedores, setProveedor]=useState([]);
  const cargarProveedor = () =>{
          fetch('https://midulceonline-candyparty.herokuapp.com/apirest/proveedor/')
              .then(res => res.json())
              .then(todosProv =>setProveedor(todosProv));
  }
  cargarProveedor();
    return (
      proveedores.map(cadaProv =>{
    return(
        <div className="proveedores-card">
        <img src={cadaProv.img_prov} className="card-img" alt="..." />
        <div className="card-contenido">
          <div className="contenido-informacion">
          <h5 className="informacion-title">Nombre:</h5>
          <span>{cadaProv.nombre_prov}</span>
          </div>
          <div className="contenido-informacion">
            <h5 className="informacion-title">Telefono:</h5>
            <span> {cadaProv.tlf_prov}</span>
            </div>
            <div className="contenido-informacion">
            <h5 className="informacion-title">Direccion:</h5>
            <span> {cadaProv.direccion_prov}</span>
            </div>
            <div className="contenido-informacion">
            <h5 className="informacion-title">Ciudad:</h5>
            <span> {cadaProv.ciudad_prov}</span> 
            </div>
            </div>
        </div>
)
})
    
    );
}
export default Proveedor;