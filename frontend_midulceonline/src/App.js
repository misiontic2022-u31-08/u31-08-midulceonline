import "./css/reset.css";
import "./index.css";
import "./css/App.css";
import "./css/header.css";
import "./css/indexPage.css";
import "./css/footer.css";
import "./css/productos.css";
import "./css/categorias.css";
import "./css/proveedor.css";
import "./css/responsive.css";
import Header from "./components/header";
import ProductosPage from "./pages/productos";
import ProveedoresPage from "./pages/proveedores";
import CategoriasPage from "./pages/categorias";
import IndexPage from "./pages/indexpage";
import Footer from "./components/footer";
import { Switch, Route } from "react-router-dom";
function App() {
  return (
    <div className="App">
      <Header />

      <div>
        <Switch>
          <Route exact path="/" component={IndexPage} />
          <Route exact path="/listaProductos" component={ProductosPage} />
          <Route exact path="/listaProveedores" component={ProveedoresPage} />
          <Route exact path="/listaCategorias" component={CategoriasPage} />
        </Switch>
      </div>

      <Footer />
    </div>
  );
}

export default App;
